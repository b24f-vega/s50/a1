import CourseCard from '../components/CourseCard'
import coursesData from '../database/courses'
import { Fragment } from 'react'
export default function Courses(){

    const coursesVariable = coursesData.map(course => {
        return(     
            <CourseCard key= {course.id} courseProp = {course}/>
        )
    })
    return(
        <Fragment>
            <h1 className='mt-3 text-center'>Courses</h1>
            {coursesVariable}
        </Fragment>
        )
}