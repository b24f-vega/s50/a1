import { Fragment } from "react";
import { Link } from "react-router-dom";

export default function NotFound() {
    return (
        <Fragment >
        <h1 className="text-center ">Oops! You seem to be lost.</h1>
        <p>Head back to :</p>
        <Link to='/'>Home</Link>
        </Fragment>
    )
}