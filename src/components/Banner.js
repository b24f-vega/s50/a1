import {Row,Col,Button} from 'react-bootstrap'
import Login from '../pages/Login'
import { Link } from 'react-router-dom'
export default function Banner(){
    return(
            <Row>
                <Col className='text-center'>
                    <h1>ZUITT Coding Bootcamp</h1>
                    <p className='pt-1'>Opportunities everywhere, Anywhere</p>
                    <Button className='pt-1 mb-5' as={Link} to='/Courses' > Enroll Now!</Button>
                </Col>
            </Row>
    )
}