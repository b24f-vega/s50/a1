import { Fragment, useContext} from 'react';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { Link,NavLink } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavBar(){

  // const [user,setUser] = useState(localStorage.getItem('email'))
  const {user} = useContext(UserContext);

    return (
    <Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand as={Link} to='/'>Zuitt</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto">
            <Nav.Link as={NavLink} to ='/'>Home</Nav.Link>
            <Nav.Link as={NavLink} to ='Courses'>Courses</Nav.Link>
            
            {
              user?
              <Nav.Link as={NavLink} to ='Logout'>Logout</Nav.Link>
              :
              <Fragment>
                  <Nav.Link as={NavLink} to ='Register'>Register</Nav.Link>
                  <Nav.Link as={NavLink} to ='Login'>Login</Nav.Link>
              </Fragment>
            }
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
    )
    
}